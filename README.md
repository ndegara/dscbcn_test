# Before you start
* Please clone the public repository we have created for this test:
<https://gitlab.com/ndegara/dscbcn_test>
* Create a new **private** repository for your code in GitLab (register free) with name: *dscbcn_your_last_name_and_name*
* Push the public test repository to this new **private** remote. After cloning the public repo:
```
git clone https://gitlab.com/ndegara/dscbcn_test
cd dscbcn_test
git remote rename origin old-origin
git remote add origin https://gitlab.com/your_username/dscbcn_your_last_name_and_name.git
git push -u origin --all
```
* Make sure you do not work on the public test repository but on your own repository, otherwise your work will be publicly available
* Finally, please invite Norberto and Enric as **mantainers** to your private repository so as we can check and track your work. Our user names in GitLab are @ndegara and @spagnoloe

# Introduction

The goal of the following exercises is to get to know your working style, technical skills and analytical thinking. Achieving a very high performance of the methods you propose is not the main goal, we would like to get to know the way you work and your skills.

Feel free to use as many resources and tools as you consider necessary. Also, please document your work. You can select any tool you consider useful for that purpose. For example you might consider using:
* Markdown or any other document discussing decisions and results
* R markdown (or similar) and generating and html output: https://rmarkdown.rstudio.com/
* Jupyter Notebooks: https://www.youtube.com/watch?v=SXBxKe8sK6I
* ...

Make sure to be concise.

Please do not hesitate to contact us in case you have any question. Our contact details have been sent to you.

## Conversion Rate Estimation

As discussed during our face-to-face interview, one of the forecasting problems we
are dealing with is the estimation of Conversion Rate (i.e., the ratio between
bookings/clicks), noted as CVR.

In this exercise we are going to provide you with 3 months (Sep - Oct - Nov 2018) of
data including the daily clicks, bookings, impressions... for a certain set of hotels
in Google metasearch channel.
Particularly, our data is aggregated at the following level:
* report date: date of the data
* account: you can interpret it as the hotel chain (Hilton, NH, ...)
* hotel
* user country: the country of the user who made a search in this hotel on that date.
* user device: the device of the user who made a search in this hotel on that date. Examples are Desktop (PC), Mobile, Tablet...

You can read more details about the data in the [Apendix](##Apendix) section below.

A placement is defined as the combination of account + hotel + user country + user
device. **Your task is to build a model for estimating the CVR on the next day at
placement level (a time series prediction problem)**. Since you are estimating the
CVR for the next date, you can obviously only use data until 1 day prior to the
day you want to estimate. For example, if you are predicting the CVR of hotel
*H* on *Desktop* device and *UK* country for the 14th of October, you can only
use the data until the 13th of October.

In order to do so, first you need to think on the following issues. Feel free to
explore the data before deciding how to approach them:
* What would be your predicted target? Would you try to predict the daily CVR? Would you try to smooth this signal somehow? Why?<br/>
**Note:** Most of the placements in the data have no clicks everyday. This means
computing the CVR for every day is tricky since you cannot measure it if there are
no clicks. Also, bear in mind that account managers evaluate the performance of
a hotel with certain time window (week, month, etc). They would never check its
performance every day.
* To compare your model you need a baseline. Which baseline model are you going to use? Make sure the baseline is meaningful but not too sophisticated.
* How will you tune the hyperparameters of your model?
* How will you compare your model with the baseline? How will you decide if your model is better?

Please justify your decisions along your work so as we can follow your work. Make sure that
your code eventually follows the approach that you planned when answering those
questions. If, when developing the code, there is something that you decide not
to do (or do in a different manner), update the explanation such that we know
that when checking the code.

After you have come a conclusion on those matters you can proceed to coding.
Choose the language you feel more comfortable with. We suggest either using *R* or *Python*.

Please make sure we can reproduce your code. For that we suggest you pack your
environment using [Anaconda](https://www.anaconda.com/distribution/) or even [Docker](https://www.docker.com/) (note that Docker images can be shared publicly
with [DockerHub](https://hub.docker.com/)).

Also use this git repository to maintain your code. Use it as you would on a work basis (branching, meaningful commit messages, etc).

#### What we will be evaluating

Our goals with this exercise are to understand:
* Your analytical skills when it comes to modelling:
  * Define well the predicted target considering the business needs.
  * Use the right tools and libraries.
  * Generate valuable features for the model (consider how much time you have, we know this can be very time consuming so keep it simple).
  * Always explain methodologies and justify everything you do.
  * Evaluate and compare models fairly.
  * Suggest future steps (new features to be added, new models to be tested, ...)
* Your programming skills:
  * Document your code as you would on a daily basis.
  * Use an appropriate coding style.
  * Be aware that code is read much more often than is written.
* Your skills when when it comes to use git and tools:
  * Please commit your work as necessary.
  * Also, branch your work at your convenience.
* Your ability to look for information and present results.

**Make sure your work focuses on these topics. Do not spend too much time on extra
topics not listed in here.**

## Appendix

The data for this problem can be downloaded in [here](https://drive.google.com/file/d/1vFcqFmOPZykzRJIuE0tgRpFX_T6vacHE/view?usp=sharing).
It is a compressed csv file containing the following data:
* Aggregation fields (note that sensitive data like the hotel name and the account
has been encoded. Still it can be used as factor fields):
  * date
  * account
  * hotel
  * user country: The country of the user.
  * user device: The device of the user.
  * hotel_country: The country of the hotel
  * hotel_city: The city of the hotel
* Metrics:
  * avg_los: The average length of stay (LOS) of the user requests (LOS is the difference between checkin and checkout dates).
  * avg_abw: The average advanced booking window (ABW) of the user requests (ABW is the difference between checkin and date, i.e., anticipation).
  * imprs_def_date: Number of impressions where the checkin and checkout dates were not selected by the user but for the channel instead.<br/>
  **Note:** An impression is registered when your offer is displayed in a user search.
  * imprs_sel_date: Number of impressions where the checkin and checkout dates were selected by the user.
  * imprs_price_bucket_neg: Number of impressions where the bid was below a channel threshold.
  * imprs_price_bucket_0: Number of impressions where the price of the hotel website was the lowest offer.
  * imprs_price_bucket_1: Number of impressions where the price of the hotel website was competitive but not the lowest offer.
  * imprs_price_bucket_2: Number of impressions where the price of the hotel website was not competitive with respect to other offers.
  * avg_ad_pos: Average ad position of the hotel website in the channel ranking.
  * clicks: Number of clicks
  * bookings: Number of bookings.

**Please feel free to filter out some account data if the amount of data you have is too large for the computation power of your system.**

[Note]: This data has been stored without index columns, no quotes for strings and NA values are stored as empty strings.
You can load the data as follows:
* Python
```python
import pandas as pd
df = pd.read_csv("performance_data_channel_20181101_20181231.csv.gz", na_values = "")
```
* R
```R
df <- read.csv("performance_data_channel_20181101_20181231.csv.gz", na.strings = "", stringsAsFactors = F)
```
